void calculateCombination(
    int numberOfBuyers, int remain, List<String> combinations, int startIndex) {
  if (remain < 0) return;

  if (remain == 0) {
    final result = StringBuffer();
    int count = 0;
    for (int i = 0; i < combinations.length; i++) {
      count += int.parse(combinations[i]);
      result.write('-${combinations[i]}');
      if (count == numberOfBuyers) break;
    }
    print(result.toString().substring(1, result.length));
    return;
  }

  for (int i = 1; i <= 2; i++) {
    combinations[startIndex] = '$i';
    calculateCombination(
        numberOfBuyers, remain - i, combinations, startIndex + 1);
  }
}

void showPossibleCombination(int numberOfBuyers) {
  List<String> combinations = List.generate(numberOfBuyers, (_) => '');
  for (int i = 1; i <= 2; i++) {
    combinations[0] = '$i';
    calculateCombination(numberOfBuyers, numberOfBuyers - i, combinations, 1);
  }
}

void main() {
  showPossibleCombination(3);
}